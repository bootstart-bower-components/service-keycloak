
window._keycloak = Keycloak('app/config/keycloak.json');

function initKeycloak() {
  var options = {
    onLoad: 'check-sso',
    openSpecs: 'allowinlinemediaplayback=yes,toolbar=no,zoom=no,clearcache=no,clearsessioncache=no'
  };
  if (localStorage.getItem("keycloakToken") !== null && localStorage.getItem("keycloakToken") !== 'undefined') {
    options.token = window.localStorage.getItem('keycloakToken');
    options.refreshToken = window.localStorage.getItem('keycloakRefreshToken');
    options.idToken = window.localStorage.getItem('keycloakIdToken');
  }
  window._keycloak
    .init(options)
    .success(function() {
      angular.bootstrap(document, ['your-app'], {
        strictDi: true
      });
    });
}

if (window.cordova) {
  document.addEventListener("deviceready", function() {
    initKeycloak();
  }, false);
}
else {
  angular.element(document).ready(function() {
    initKeycloak();
  });
}
