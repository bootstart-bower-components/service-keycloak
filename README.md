Keycloak
========

# Description
This module provide a set of tools to integrate keycloak in a frontend.
It contains a service (`keycloakService`) to communicate with keycloak, 
an interceptor (`keycloakInterceptor`) to add the token in the backend requests
and an RBAC route policy (`keycloakRBacService`).

# Install

    bower install git@gitlab.com:bootstart-bower-components/service-keycloak.git --save

Then add `bootstart.keycloak` in your index.module.js

# Configuration

## Configuration of the backend
In the `webapp/WEB-INF/web.xml` file, add `Client-Id` in the list of the allowed headers (`allowedHeaders`).

To secure your REST API, use the class `RBACRoutePolicy` of `bootstart-auth-client`.
It is made to work with this module.

## Configuration of the keycloak service
In your `app/config` folder add a `keycloakInit.js` file.
An exemple for cordova and for a normal app is in the `example` folder.

In this file, replace `your-app` by the name of your app as defined in the `index.module.js`.

You have two choices for the `onLoad` :
+ `check-sso` : If you have some public page
+ `login-required` : If you want to force to be authenticated to access to all pages

The `openSpecs` attribute is useful on mobile to configure the way the popup is displayed.
For more informations about the options see : https://cordova.apache.org/docs/fr/3.1.0/cordova/inappbrowser/window.open.html


Remember to import this file in your `index.module.js` in a normal app with :
```javascript
import './config/keycloackInit';
```

and in your `index.html` in a mobile app with :
```html
<script src="dist/keycloakInit.js"></script>
```

You also need to remove the attribute `ng-app` in your `index.html` to avoid infinite redirection loop.

---
Then, in your Keycloak client settings, make sure the `Access Type` is `Confidential`.

Go in the tab `Installation` and download the install file in the format `Keycloak OIDC Json`.
Put this file in the `app/config` folder.

---
On ios, the tokens are not saved automatically. To do it, add `keycloakServiceProvider` 
in the dependencies of the config function. Then, you can set the backend url with :

```javascript
keycloakServiceProvider.setSaveInLocalStorage(true);
```


## Configuration of the interceptor
Add `keycloakInterceptorProvider` in the dependencies of the config function.
Then, you can set the backend url with :

```javascript
keycloakInterceptorProvider.setBackendUrl(YOUR_URL);
```

## Configuration of the RBAC route policy

### Restrict the access to pages
If you want to restrict some pages for users with a given role, 
add `keycloakRBACServiceProvider` in the dependencies of the config function.
Then, you can set the state where the user will be redirected if it doesn't 
has the right to access to a restricted page with:

```javascript
keycloakRBACServiceProvider.setDeniedState('forbiddenState');
```

Finally, add `keycloakRBACService` in the dependencies of the run function
and add in the function : 

```javascript
keycloakRBACService.start();
```

The configuration of the necessary roles for each state is made in the `index.route.js` file.
It is made with an attribute `access` in the state object. 

Example : 
``` javascript
$stateProvider.state('personalInfo', {
    url: '/account/personalInfo',
    templateUrl: 'app/pages/personalInfo/personalInfo.html',
    controller: "PersonalInfoController",
    controllerAs: "ctrl",
    access: {
        requiredLogin: true,
        requiredRole: 'user'
    }
})
```

+ If `access` is not present or if `requiredLogin` is `false`, the page is public
+ If required role is empty of not present, the page is restricted to authenticated users
+ The role can be static (with a simple string like in the example) or dynamic.
+ A dynamic role is like `{company}-admin` where `{company}` will be replaced in the check
by the attribute in the token named `company`.
+ The require role can contains OR rules like `user|admin`. In that case if the user has the user role or the admin role
he can access the page.

### Restrict the access to resources

It is also possible to restrict the access to some resources in a page. 
To do this, in the config function, set the required roles for each resource
with `keycloakRBACServiceProvider.setResourcesAccess`. 

Example : 
``` javascript
keycloakRBACServiceProvider.setResourcesAccess({
    resource1 : ['requiredRole1', 'requiredRole2', ...], //The , means a OR not an AND
    resource2 : [...]
})
```

Then you can use the directive `bootstart-has-access` in every html entity to check
if the the user can see the given resource. The parameter of this directive is the role. 

`Warning : ` You cannot use this directive with the `ng-if` directive. 

Example :
``` html
<div bootstart-has-access="resource1"></div>
```

# Methods of keycloakService

## authenticate (options)
    
#### Description

Show the keycloak page (if the user is not authenticated) to let the user
authenticate himself.

#### Parameters

###### options :

Options sent to keycloak. See https://keycloak.gitbooks.io/securing-client-applications-guide/content/v/2.0/topics/oidc/javascript-adapter.html#_login_options for more informations.

## getToken()
    
#### Description

Return the token of the user.

#### Return value

The token as a string or `undefined`.

## isAuthenticated()

Return `true` if the user is authenticated, otherwise `false.

## hasRealmRole(role)

Return the `true` if the user has the given realm role, otherwise `false`.

## hasResourceRole(role, resource)

#### Description

Return the `true` if the user has the given resource role, otherwise `false`.

#### Parameters

###### role :

Role to check (it is the name of the role, not the `id`)

###### resource :

Resource where the role is (it is for role defined in a client).

## logout ()

Clear the token and logout the user. If `onLoad` is defined to `login-required`
in the `keycloakInit.js` file, it will redirect to the login page of keycloak.

## register()

Show the keycloak page (if the user is not authenticated) to let the user
register himself.

## tokenParsed()
    
#### Description

Return the token parsed of the user.

#### Return value

The token as an object or `undefined`.

## updateAttribute(attribute, value, id = null)
    
#### Description

Update the attribute with the given name of the given user.

The user has to have the keycloak role `manage-users`. That's why, it is better
to proxy this request with a backend and use `backendService`.

#### Parameters

###### attribute (`required`) :

Name of the attribute to update.

###### value (`required`) :

Value of the attribute to update.

###### id :

If present, it updates the attribute of the user with the given id, otherwise, 
it updates the attribute of the current user.

#### Return value

It returns a `$promise` to let you recover the keycloak response with the `then` method.

#### Example

```javascript
keycloakService.updateAttribute('isInitialized', true).then((response) => {
    console.log('success');    
}, (error) => {
    console.log('error');    
});
```

## updateAccountInformations(userInfo, id = null)
    
#### Description

Update the info the given user.

The user has to have the keycloak role `manage-users`. That's why, it is better
to proxy this request with a backend and use `backendService`.

#### Parameters

###### userInfo (`required`) :

Info to update.
The structure of the object has to be like : 
```javascript
{
    email: 'test@boot-start.com',
    firstName: 'Jean',
    lastName: 'Dupont',
    pseudo: 'test'
}
```

###### id :

If present, it updates the info of the user with the given id, otherwise, 
it updates the info of the current user.

#### Return value

It returns a `$promise` to let you recover the keycloak response with the `then` method.

#### Example

```javascript
keycloakService.updateAccountInformations(user).then((response) => {
    console.log('success');    
}, (error) => {
    console.log('error');    
});
```

## updatePassword(password, id = null)
    
#### Description

Update the password of the given user.

The user has to have the keycloak role `manage-users`. That's why, it is better
to proxy this request with a backend and use `backendService`.

#### Parameters

###### password (`required`) :

New password of the user.

###### id :

If present, it updates the password of the user with the given id, otherwise, 
it updates the password of the current user.

#### Return value

It returns a `$promise` to let you recover the keycloak response with the `then` method.

#### Example

```javascript
keycloakService.updatePassword('azertyuiop').then((response) => {
    console.log('success');    
}, (error) => {
    console.log('error');    
});
```

## updateToken(minValidity = 0)
   
#### Description

It updates the token if necessary.

#### Parameters

###### minValidity (`required`) :

If the token is not valid for at least `minValidity` seconds, it updates the
token.

#### Return value

It returns a `$promise` to let you recover the keycloak response with the `then` method.

## keycloakApiGet(type, id, action, actionId = null)
    
#### Description

It makes a GET request on the keycloak admin API. See http://www.keycloak.org/docs/rest-api/index.html
for the documentation of the rest API.

The url of the GET is `keycloakAdminAPIURL/:type/:id/:action/:actionId`.

The user has to have some admin keycloak role. That's why, it is better
to proxy this request with a backend and use `backendService`.

#### Parameters

###### type (`required`) :

Type of the resource to get (users, groups, ...).

###### id (`required`) :

Id of the resource to get.

###### action (`required`) :

Action to perform on the resource.

###### actionId :

Id of the action.

#### Return value

It returns a `$promise` to let you recover the keycloak response with the `then` method.

## keycloakApiPut(type, id, action, data, actionId = null)
    
#### Description

It makes a PUT request on the keycloak admin API. See http://www.keycloak.org/docs/rest-api/index.html
for the documentation of the rest API.

The url of the PUT is `keycloakAdminAPIURL/:type/:id/:action/:actionId`.

The user has to have some admin keycloak role. That's why, it is better
to proxy this request with a backend and use `backendService`.

#### Parameters

###### type (`required`) :

Type of the resource to update (users, groups, ...).

###### id (`required`) :

Id of the resource to update.

###### action (`required`) :

Action to perform on the resource.

###### data (`required`) :

JSON object to put in the request body.

###### actionId :

Id of the action.

#### Return value

It returns a `$promise` to let you recover the keycloak response with the `then` method.