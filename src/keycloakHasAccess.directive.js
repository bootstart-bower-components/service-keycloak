export function keycloakHasAccess(ngIfDirective, keycloakRBACService) {
    'ngInject';
    var ngIf = ngIfDirective[0];

    return {
        transclude: ngIf.transclude,
        priority: ngIf.priority,
        terminal: ngIf.terminal,
        restrict: ngIf.restrict,
        link: function ($scope, $element, $attr) {
            var hasAccess = keycloakRBACService.hasAccessToResource($attr.bootstartHasAccess);
            $attr.ngIf = function () {
                return hasAccess;
            };
            ngIf.link.apply(ngIf, arguments);
        }
    };
};
