
export class KeycloakInterceptor {
    constructor($q, $injector, backendUrl) {
        this.$q = $q;
        this.$injector = $injector;
        this.backendUrl = backendUrl;
        this.keycloakService = null;

        return {
            request: this.request.bind(this)
        };
    }


    static create($q, $injector, REST_BACKEND_URL) {
        return new KeycloakInterceptor($q, $injector, REST_BACKEND_URL);
    }

    request (config) {
        if (this.keycloakService === null) {//To avoid circular dependancies
            this.keycloakService = this.$injector.get('keycloakService');
        }
        if (config.url.indexOf(this.backendUrl) > -1 || config.url.indexOf(this.keycloakService.keycloakUrl) > -1) {
            config.headers.Authorization = this.keycloakService.getTokenHeader();
            config.headers.Accept = "application/json";
            if (config.url.indexOf(this.backendUrl) > -1) {
                config.headers['Client-Id'] = this.keycloakService.keycloak.clientId;
            }
        }
        return config;
    }
}

export class KeycloakInterceptorProvider {
    constructor($httpProvider) {
        'ngInject';
        this.$httpProvider = $httpProvider;
    }

    setBackendUrl(url) {
        this.backendUrl = url;
        this.$httpProvider.interceptors.push('keycloakInterceptor');
    }

    $get ($q, $injector) {
        'ngInject';
        return new KeycloakInterceptor($q, $injector, this.backendUrl);
    }
}
