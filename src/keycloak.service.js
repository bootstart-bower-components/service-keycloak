
import {KeycloakInterceptorProvider} from './keycloakInterceptor.factory';
import {KeycloakRBACServiceProvider} from './keycloakRBAC.service';
import {keycloakHasAccess} from './keycloakHasAccess.directive';


export class KeycloakService {
    constructor($resource, $q, $rootScope, saveInLocalStorage) {

        this.keycloak = window._keycloak;
        this.$q = $q;
        this.saveInLocalStorage = saveInLocalStorage;
        this.userInfo = {
            email: this.tokenParsed().email,
            firstName: this.tokenParsed().given_name,
            lastName: this.tokenParsed().family_name,
            pseudo: this.tokenParsed().preferred_username,
            id: this.keycloak.subject
        };

        this.$resource = $resource;
        this.$rootScope = $rootScope;
        this.keycloakUrl = this.keycloak.authServerUrl;
        this.keycloakAdminResource = $resource(this.keycloakUrl + '/admin/realms/' + this.keycloak.realm + '/:type/:id/:action/:actionId', {}, {
            put : {method: 'PUT', isArray: true},
            get : {method: 'GET', isArray: true}
        });
    }

    authenticate(option) {
        if (!this.keycloak.authenticated) {
            this.keycloak.login(option).success((response) => {
                this.onAuthSuccess();
            }).error((data, status) => {
                this.onAuthError();
            });
        }
        else {
            this.onAuthSuccess();
        }
    }

    onAuthSuccess () {
        this.userInfo.email = this.tokenParsed().email;
        this.userInfo.pseudo = this.tokenParsed().preferred_username;
        this.userInfo.firstName = this.tokenParsed().given_name;
        this.userInfo.lastName = this.tokenParsed().family_name;

        this.userInfo.id = this.keycloak.subject;
        if (this.saveInLocalStorage) {
            window.localStorage.setItem('keycloakToken', this.keycloak.token);
            window.localStorage.setItem('keycloakRefreshToken', this.keycloak.refreshToken);
            window.localStorage.setItem('keycloakIdToken', this.keycloak.idToken);
        }
        this.$rootScope.$broadcast("keycloakAuthenticated");
        this.loadUserInfo();
    }

    onAuthError () {
        this.$rootScope.$broadcast("keycloakAuthenticationFailed");
    }

    getToken() {
        return this.keycloak.token;
    }

    getHeaders(addClientIdHeader = true, addToken = true) {
        var headers = {
            "Accept": "application/json"
        };
        if (addClientIdHeader) {
            headers['Client-Id'] = this.keycloak.clientId;
        }
        if (addToken) {
            headers['Authorization'] = this.getTokenHeader();
        }
        return headers;
    }

    getTokenHeader() {
        return "bearer " + this.keycloak.token;
    }

    isAuthenticated() {
        return this.keycloak.authenticated;
    }

    hasRealmRole(role) {
        return this.keycloak.hasRealmRole(role);
    }

    hasResourceRole(role, resource) {
        return this.keycloak.hasResourceRole(role, resource);
    }

    logout() {
        this.userInfo = {};
        if (this.saveInLocalStorage) {
            window.localStorage.removeItem('keycloakToken');
            window.localStorage.removeItem('keycloakRefreshToken');
            window.localStorage.removeItem('keycloakIdToken');
        }
        this.keycloak.logout();
    }

    loadUserInfo() {
        this.keycloak.loadUserProfile().success((response) => {
                this.$rootScope.$apply(() => {
                this.userInfo.email = (response.email) ? response.email : "";
                this.userInfo.firstName = (response.firstName) ? response.firstName : "";
                this.userInfo.lastName = (response.lastName) ? response.lastName : "";
                this.userInfo.pseudo = (response.username) ? response.username : "";
                this.userInfo.id = (response.id) ? response.id : "";
                this.userInfo.attributes = (response.attributes) ? response.attributes : {};
            });
            this.$rootScope.$broadcast("keycloakInfoLoadedCallback", response);
        });
    }

    register() {
        this.keycloak.register().success((response) => {
            this.onAuthSuccess();
        }).error((data, status) => {
            this.onAuthError();
        });
    }

    tokenParsed() {
        if (angular.isDefined(this.keycloak.tokenParsed)) {
            return this.keycloak.tokenParsed;
        }
        return {};
    }

    updateAttribute(attribute, value, id = null) {
        this.updateToken(60);
        var attributes = this.userInfo.attributes;
        attributes[attribute] = ["" + value];

        id = (id === null) ? this.userInfo.id : id;
        return this.keycloakApiPut('users', id, null, {
                attributes: attributes
            });
    }

    updateAccountInformations(userInfo, id = null) {
        this.updateToken(60);
        id = (id === null) ? this.userInfo.id : id;
        return this.keycloakApiPut('users', id, null,
            {
                username: userInfo.pseudo,
                firstName: userInfo.firstName,
                lastName: userInfo.lastName,
                email: userInfo.email
            });
    }

    updatePassword(password, id = null) {
        this.updateToken(60);
        id = (id === null) ? this.userInfo.id : id;
        return this.keycloakApiPut('users', id, 'reset-password',
            {
                type: 'password',
                temporary: false,
                value: password
            });
    }

    updateToken(minValidity = 0) {
        var deferred = this.$q.defer();

        this.keycloak.updateToken(minValidity).success(() => {

            if (this.saveInLocalStorage)
            {
                var keycloakToken = window.localStorage.getItem("keycloakToken");
                var keycloakRefreshToken = window.localStorage.getItem("keycloakRefreshToken");
                var keycloakIdToken = window.localStorage.getItem("keycloakIdToken");

                if (keycloakToken !== null && keycloakToken !== 'undefined') {
                    window.localStorage.setItem('keycloakToken', this.keycloak.token);
                }
                if (keycloakRefreshToken !== null && keycloakRefreshToken !== 'undefined') {
                    window.localStorage.setItem('keycloakRefreshToken', this.keycloak.refreshToken);
                }
                if (keycloakIdToken !== null && keycloakIdToken !== 'undefined') {
                    window.localStorage.setItem('keycloakIdToken', this.keycloak.idToken);
                }
            }
            deferred.resolve();
        }).error(() => {
            this.$rootScope.$broadcast("keycloakTokenInvalid");
            deferred.reject();
        });

        return deferred.promise;
    }

    keycloakApiGet(type, id, action, actionId = null) {
        return this.keycloakAdminResource.get({
            type: type,
            id: id,
            action: action,
            actionId: actionId
        }).$promise;
    }

    keycloakApiPut(type, id, action, data, actionId = null) {
        return this.keycloakAdminResource.put({
                type: type,
                id: id,
                action: action,
                actionId: actionId
            }, data).$promise;
    }
}

export class KeycloakServiceProvider {
    constructor() {
        this.saveInLocalStorage = false;
    }

    setSaveInLocalStorage(saveInLocalStorage) {
        this.saveInLocalStorage = saveInLocalStorage;
    }

    $get ($resource, $q, $rootScope) {
        'ngInject';
        return new KeycloakService($resource, $q, $rootScope, this.saveInLocalStorage);
    }
}


angular.module('bootstart.keycloak', ['ngResource'])
    .directive('bootstartHasAccess', keycloakHasAccess)
    .provider('keycloakService', KeycloakServiceProvider)
    .provider('keycloakRBACService', KeycloakRBACServiceProvider)
    .provider('keycloakInterceptor', KeycloakInterceptorProvider);
