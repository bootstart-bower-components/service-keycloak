/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(3);


/***/ },
/* 1 */,
/* 2 */,
/* 3 */
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	var KeycloakRBACService = function () {
	    function KeycloakRBACService($injector, $state, $rootScope, $timeout, deniedState, resourcesAccess) {
	        _classCallCheck(this, KeycloakRBACService);

	        if (this.keycloakService === null) {
	            //To avoid circular dependancies
	            this.keycloakService = this.$injector.get('keycloakService');
	        }
	        this.keycloakService = null;
	        this.resourcesAccess = resourcesAccess;
	        this.$injector = $injector;
	        this.$state = $state;
	        this.$timeout = $timeout;
	        this.$rootScope = $rootScope;
	        this.deniedState = deniedState;
	    }

	    _createClass(KeycloakRBACService, [{
	        key: 'start',
	        value: function start() {
	            var _this = this;

	            this.$rootScope.$on('$stateChangeStart', function (event, toState) {
	                try {
	                    _this.checkAccess(toState.access);
	                } catch (err) {
	                    console.log("Error during the role checking");
	                    _this.$state.go(_this.deniedState);
	                }
	            });
	        }
	    }, {
	        key: 'checkAccess',
	        value: function checkAccess(access) {
	            var _this2 = this;

	            if (this.keycloakService === null) {
	                //To avoid circular dependancies
	                this.keycloakService = this.$injector.get('keycloakService');
	            }

	            //Public access
	            if (angular.isUndefined(access) || angular.isUndefined(access.requiredLogin) || !access.requiredLogin) {
	                return;
	            }
	            //Authenticated but no required role
	            if (this.keycloakService.isAuthenticated() && angular.isUndefined(access.requiredRole)) {
	                return;
	            }

	            var roles = access.requiredRole.split('|');
	            for (var i = 0; i < roles.length; ++i) {
	                var role = roles[i].replace(/{(.*)}/, function (match, p1) {
	                    return _this2.keycloakService.tokenParsed()[p1];
	                });

	                //Realm role access
	                if (this.keycloakService.hasRealmRole(role)) {
	                    return;
	                }

	                //Client role access
	                if (this.keycloakService.hasResourceRole(role)) {
	                    return;
	                }
	            };

	            //If the user is authenticate, we go to the deniedState
	            if (this.keycloakService.isAuthenticated()) {
	                this.$timeout(function () {
	                    _this2.$state.go(_this2.deniedState);
	                });
	            } else {
	                this.keycloakService.authenticate();
	            }
	        }
	    }, {
	        key: 'hasAccessToResource',
	        value: function hasAccessToResource(resource) {
	            if (this.keycloakService === null) {
	                //To avoid circular dependancies
	                this.keycloakService = this.$injector.get('keycloakService');
	            }

	            var acceptedRoles = this.resourcesAccess[resource];
	            if (angular.isUndefined(acceptedRoles)) {
	                return false;
	            }

	            for (var i = 0; i < acceptedRoles.length; ++i) {
	                if (this.keycloakService.hasRealmRole(acceptedRoles[i])) {
	                    return true;
	                }
	            }
	            return false;
	        }
	    }]);

	    return KeycloakRBACService;
	}();

	var KeycloakRBACServiceProvider = exports.KeycloakRBACServiceProvider = function () {
	    function KeycloakRBACServiceProvider() {
	        _classCallCheck(this, KeycloakRBACServiceProvider);

	        this.deniedState = '';
	        this.resourcesAccess = {};
	    }

	    _createClass(KeycloakRBACServiceProvider, [{
	        key: 'setDeniedState',
	        value: function setDeniedState(deniedState) {
	            this.deniedState = deniedState;
	        }
	    }, {
	        key: 'setResourcesAccess',
	        value: function setResourcesAccess(resourcesAccess) {
	            this.resourcesAccess = resourcesAccess;
	        }
	    }, {
	        key: '$get',
	        value: ["$injector", "$state", "$rootScope", "$timeout", function $get($injector, $state, $rootScope, $timeout) {
	            'ngInject';

	            return new KeycloakRBACService($injector, $state, $rootScope, $timeout, this.deniedState, this.resourcesAccess);
	        }]
	    }]);

	    return KeycloakRBACServiceProvider;
	}();

/***/ }
/******/ ]);