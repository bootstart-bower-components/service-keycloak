/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(1);


/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.KeycloakServiceProvider = exports.KeycloakService = undefined;

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	var _keycloakInterceptor = __webpack_require__(2);

	var _keycloakRBAC = __webpack_require__(3);

	var _keycloakHasAccess = __webpack_require__(4);

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	var KeycloakService = exports.KeycloakService = function () {
	    function KeycloakService($resource, $q, $rootScope, saveInLocalStorage) {
	        _classCallCheck(this, KeycloakService);

	        this.keycloak = window._keycloak;
	        this.$q = $q;
	        this.saveInLocalStorage = saveInLocalStorage;
	        this.userInfo = {
	            email: this.tokenParsed().email,
	            firstName: this.tokenParsed().given_name,
	            lastName: this.tokenParsed().family_name,
	            pseudo: this.tokenParsed().preferred_username,
	            id: this.keycloak.subject
	        };

	        this.$resource = $resource;
	        this.$rootScope = $rootScope;
	        this.keycloakUrl = this.keycloak.authServerUrl;
	        this.keycloakAdminResource = $resource(this.keycloakUrl + '/admin/realms/' + this.keycloak.realm + '/:type/:id/:action/:actionId', {}, {
	            put: { method: 'PUT', isArray: true },
	            get: { method: 'GET', isArray: true }
	        });
	    }

	    _createClass(KeycloakService, [{
	        key: 'authenticate',
	        value: function authenticate(option) {
	            var _this = this;

	            if (!this.keycloak.authenticated) {
	                this.keycloak.login(option).success(function (response) {
	                    _this.onAuthSuccess();
	                }).error(function (data, status) {
	                    _this.onAuthError();
	                });
	            } else {
	                this.onAuthSuccess();
	            }
	        }
	    }, {
	        key: 'onAuthSuccess',
	        value: function onAuthSuccess() {
	            this.userInfo.email = this.tokenParsed().email;
	            this.userInfo.pseudo = this.tokenParsed().preferred_username;
	            this.userInfo.firstName = this.tokenParsed().given_name;
	            this.userInfo.lastName = this.tokenParsed().family_name;

	            this.userInfo.id = this.keycloak.subject;
	            if (this.saveInLocalStorage) {
	                window.localStorage.setItem('keycloakToken', this.keycloak.token);
	                window.localStorage.setItem('keycloakRefreshToken', this.keycloak.refreshToken);
	                window.localStorage.setItem('keycloakIdToken', this.keycloak.idToken);
	            }
	            this.$rootScope.$broadcast("keycloakAuthenticated");
	            this.loadUserInfo();
	        }
	    }, {
	        key: 'onAuthError',
	        value: function onAuthError() {
	            this.$rootScope.$broadcast("keycloakAuthenticationFailed");
	        }
	    }, {
	        key: 'getToken',
	        value: function getToken() {
	            return this.keycloak.token;
	        }
	    }, {
	        key: 'getHeaders',
	        value: function getHeaders() {
	            var addClientIdHeader = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;
	            var addToken = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

	            var headers = {
	                "Accept": "application/json"
	            };
	            if (addClientIdHeader) {
	                headers['Client-Id'] = this.keycloak.clientId;
	            }
	            if (addToken) {
	                headers['Authorization'] = this.getTokenHeader();
	            }
	            return headers;
	        }
	    }, {
	        key: 'getTokenHeader',
	        value: function getTokenHeader() {
	            return "bearer " + this.keycloak.token;
	        }
	    }, {
	        key: 'isAuthenticated',
	        value: function isAuthenticated() {
	            return this.keycloak.authenticated;
	        }
	    }, {
	        key: 'hasRealmRole',
	        value: function hasRealmRole(role) {
	            return this.keycloak.hasRealmRole(role);
	        }
	    }, {
	        key: 'hasResourceRole',
	        value: function hasResourceRole(role, resource) {
	            return this.keycloak.hasResourceRole(role, resource);
	        }
	    }, {
	        key: 'logout',
	        value: function logout() {
	            this.userInfo = {};
	            if (this.saveInLocalStorage) {
	                window.localStorage.removeItem('keycloakToken');
	                window.localStorage.removeItem('keycloakRefreshToken');
	                window.localStorage.removeItem('keycloakIdToken');
	            }
	            this.keycloak.logout();
	        }
	    }, {
	        key: 'loadUserInfo',
	        value: function loadUserInfo() {
	            var _this2 = this;

	            this.keycloak.loadUserProfile().success(function (response) {
	                _this2.$rootScope.$apply(function () {
	                    _this2.userInfo.email = response.email ? response.email : "";
	                    _this2.userInfo.firstName = response.firstName ? response.firstName : "";
	                    _this2.userInfo.lastName = response.lastName ? response.lastName : "";
	                    _this2.userInfo.pseudo = response.username ? response.username : "";
	                    _this2.userInfo.id = response.id ? response.id : "";
	                    _this2.userInfo.attributes = response.attributes ? response.attributes : {};
	                });
	                _this2.$rootScope.$broadcast("keycloakInfoLoadedCallback", response);
	            });
	        }
	    }, {
	        key: 'register',
	        value: function register() {
	            var _this3 = this;

	            this.keycloak.register().success(function (response) {
	                _this3.onAuthSuccess();
	            }).error(function (data, status) {
	                _this3.onAuthError();
	            });
	        }
	    }, {
	        key: 'tokenParsed',
	        value: function tokenParsed() {
	            if (angular.isDefined(this.keycloak.tokenParsed)) {
	                return this.keycloak.tokenParsed;
	            }
	            return {};
	        }
	    }, {
	        key: 'updateAttribute',
	        value: function updateAttribute(attribute, value) {
	            var id = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

	            this.updateToken(60);
	            var attributes = this.userInfo.attributes;
	            attributes[attribute] = ["" + value];

	            id = id === null ? this.userInfo.id : id;
	            return this.keycloakApiPut('users', id, null, {
	                attributes: attributes
	            });
	        }
	    }, {
	        key: 'updateAccountInformations',
	        value: function updateAccountInformations(userInfo) {
	            var id = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

	            this.updateToken(60);
	            id = id === null ? this.userInfo.id : id;
	            return this.keycloakApiPut('users', id, null, {
	                username: userInfo.pseudo,
	                firstName: userInfo.firstName,
	                lastName: userInfo.lastName,
	                email: userInfo.email
	            });
	        }
	    }, {
	        key: 'updatePassword',
	        value: function updatePassword(password) {
	            var id = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

	            this.updateToken(60);
	            id = id === null ? this.userInfo.id : id;
	            return this.keycloakApiPut('users', id, 'reset-password', {
	                type: 'password',
	                temporary: false,
	                value: password
	            });
	        }
	    }, {
	        key: 'updateToken',
	        value: function updateToken() {
	            var _this4 = this;

	            var minValidity = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;

	            var deferred = this.$q.defer();

	            this.keycloak.updateToken(minValidity).success(function () {

	                if (_this4.saveInLocalStorage) {
	                    var keycloakToken = window.localStorage.getItem("keycloakToken");
	                    var keycloakRefreshToken = window.localStorage.getItem("keycloakRefreshToken");
	                    var keycloakIdToken = window.localStorage.getItem("keycloakIdToken");

	                    if (keycloakToken !== null && keycloakToken !== 'undefined') {
	                        window.localStorage.setItem('keycloakToken', _this4.keycloak.token);
	                    }
	                    if (keycloakRefreshToken !== null && keycloakRefreshToken !== 'undefined') {
	                        window.localStorage.setItem('keycloakRefreshToken', _this4.keycloak.refreshToken);
	                    }
	                    if (keycloakIdToken !== null && keycloakIdToken !== 'undefined') {
	                        window.localStorage.setItem('keycloakIdToken', _this4.keycloak.idToken);
	                    }
	                }
	                deferred.resolve();
	            }).error(function () {
	                _this4.$rootScope.$broadcast("keycloakTokenInvalid");
	                deferred.reject();
	            });

	            return deferred.promise;
	        }
	    }, {
	        key: 'keycloakApiGet',
	        value: function keycloakApiGet(type, id, action) {
	            var actionId = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;

	            return this.keycloakAdminResource.get({
	                type: type,
	                id: id,
	                action: action,
	                actionId: actionId
	            }).$promise;
	        }
	    }, {
	        key: 'keycloakApiPut',
	        value: function keycloakApiPut(type, id, action, data) {
	            var actionId = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : null;

	            return this.keycloakAdminResource.put({
	                type: type,
	                id: id,
	                action: action,
	                actionId: actionId
	            }, data).$promise;
	        }
	    }]);

	    return KeycloakService;
	}();

	var KeycloakServiceProvider = exports.KeycloakServiceProvider = function () {
	    function KeycloakServiceProvider() {
	        _classCallCheck(this, KeycloakServiceProvider);

	        this.saveInLocalStorage = false;
	    }

	    _createClass(KeycloakServiceProvider, [{
	        key: 'setSaveInLocalStorage',
	        value: function setSaveInLocalStorage(saveInLocalStorage) {
	            this.saveInLocalStorage = saveInLocalStorage;
	        }
	    }, {
	        key: '$get',
	        value: ["$resource", "$q", "$rootScope", function $get($resource, $q, $rootScope) {
	            'ngInject';

	            return new KeycloakService($resource, $q, $rootScope, this.saveInLocalStorage);
	        }]
	    }]);

	    return KeycloakServiceProvider;
	}();

	angular.module('bootstart.keycloak', ['ngResource']).directive('bootstartHasAccess', _keycloakHasAccess.keycloakHasAccess).provider('keycloakService', KeycloakServiceProvider).provider('keycloakRBACService', _keycloakRBAC.KeycloakRBACServiceProvider).provider('keycloakInterceptor', _keycloakInterceptor.KeycloakInterceptorProvider);

/***/ },
/* 2 */
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	var KeycloakInterceptor = exports.KeycloakInterceptor = function () {
	    function KeycloakInterceptor($q, $injector, backendUrl) {
	        _classCallCheck(this, KeycloakInterceptor);

	        this.$q = $q;
	        this.$injector = $injector;
	        this.backendUrl = backendUrl;
	        this.keycloakService = null;

	        return {
	            request: this.request.bind(this)
	        };
	    }

	    _createClass(KeycloakInterceptor, [{
	        key: 'request',
	        value: function request(config) {
	            if (this.keycloakService === null) {
	                //To avoid circular dependancies
	                this.keycloakService = this.$injector.get('keycloakService');
	            }
	            if (config.url.indexOf(this.backendUrl) > -1 || config.url.indexOf(this.keycloakService.keycloakUrl) > -1) {
	                config.headers.Authorization = this.keycloakService.getTokenHeader();
	                config.headers.Accept = "application/json";
	                if (config.url.indexOf(this.backendUrl) > -1) {
	                    config.headers['Client-Id'] = this.keycloakService.keycloak.clientId;
	                }
	            }
	            return config;
	        }
	    }], [{
	        key: 'create',
	        value: function create($q, $injector, REST_BACKEND_URL) {
	            return new KeycloakInterceptor($q, $injector, REST_BACKEND_URL);
	        }
	    }]);

	    return KeycloakInterceptor;
	}();

	var KeycloakInterceptorProvider = exports.KeycloakInterceptorProvider = function () {
	    KeycloakInterceptorProvider.$inject = ["$httpProvider"];
	    function KeycloakInterceptorProvider($httpProvider) {
	        'ngInject';

	        _classCallCheck(this, KeycloakInterceptorProvider);

	        this.$httpProvider = $httpProvider;
	    }

	    _createClass(KeycloakInterceptorProvider, [{
	        key: 'setBackendUrl',
	        value: function setBackendUrl(url) {
	            this.backendUrl = url;
	            this.$httpProvider.interceptors.push('keycloakInterceptor');
	        }
	    }, {
	        key: '$get',
	        value: ["$q", "$injector", function $get($q, $injector) {
	            'ngInject';

	            return new KeycloakInterceptor($q, $injector, this.backendUrl);
	        }]
	    }]);

	    return KeycloakInterceptorProvider;
	}();

/***/ },
/* 3 */
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	var KeycloakRBACService = function () {
	    function KeycloakRBACService($injector, $state, $rootScope, $timeout, deniedState, resourcesAccess) {
	        _classCallCheck(this, KeycloakRBACService);

	        if (this.keycloakService === null) {
	            //To avoid circular dependancies
	            this.keycloakService = this.$injector.get('keycloakService');
	        }
	        this.keycloakService = null;
	        this.resourcesAccess = resourcesAccess;
	        this.$injector = $injector;
	        this.$state = $state;
	        this.$timeout = $timeout;
	        this.$rootScope = $rootScope;
	        this.deniedState = deniedState;
	    }

	    _createClass(KeycloakRBACService, [{
	        key: 'start',
	        value: function start() {
	            var _this = this;

	            this.$rootScope.$on('$stateChangeStart', function (event, toState) {
	                try {
	                    _this.checkAccess(toState.access);
	                } catch (err) {
	                    console.log("Error during the role checking");
	                    _this.$state.go(_this.deniedState);
	                }
	            });
	        }
	    }, {
	        key: 'checkAccess',
	        value: function checkAccess(access) {
	            var _this2 = this;

	            if (this.keycloakService === null) {
	                //To avoid circular dependancies
	                this.keycloakService = this.$injector.get('keycloakService');
	            }

	            //Public access
	            if (angular.isUndefined(access) || angular.isUndefined(access.requiredLogin) || !access.requiredLogin) {
	                return;
	            }
	            //Authenticated but no required role
	            if (this.keycloakService.isAuthenticated() && angular.isUndefined(access.requiredRole)) {
	                return;
	            }

	            var roles = access.requiredRole.split('|');
	            for (var i = 0; i < roles.length; ++i) {
	                var role = roles[i].replace(/{(.*)}/, function (match, p1) {
	                    return _this2.keycloakService.tokenParsed()[p1];
	                });

	                //Realm role access
	                if (this.keycloakService.hasRealmRole(role)) {
	                    return;
	                }

	                //Client role access
	                if (this.keycloakService.hasResourceRole(role)) {
	                    return;
	                }
	            };

	            //If the user is authenticate, we go to the deniedState
	            if (this.keycloakService.isAuthenticated()) {
	                this.$timeout(function () {
	                    _this2.$state.go(_this2.deniedState);
	                });
	            } else {
	                this.keycloakService.authenticate();
	            }
	        }
	    }, {
	        key: 'hasAccessToResource',
	        value: function hasAccessToResource(resource) {
	            if (this.keycloakService === null) {
	                //To avoid circular dependancies
	                this.keycloakService = this.$injector.get('keycloakService');
	            }

	            var acceptedRoles = this.resourcesAccess[resource];
	            if (angular.isUndefined(acceptedRoles)) {
	                return false;
	            }

	            for (var i = 0; i < acceptedRoles.length; ++i) {
	                if (this.keycloakService.hasRealmRole(acceptedRoles[i])) {
	                    return true;
	                }
	            }
	            return false;
	        }
	    }]);

	    return KeycloakRBACService;
	}();

	var KeycloakRBACServiceProvider = exports.KeycloakRBACServiceProvider = function () {
	    function KeycloakRBACServiceProvider() {
	        _classCallCheck(this, KeycloakRBACServiceProvider);

	        this.deniedState = '';
	        this.resourcesAccess = {};
	    }

	    _createClass(KeycloakRBACServiceProvider, [{
	        key: 'setDeniedState',
	        value: function setDeniedState(deniedState) {
	            this.deniedState = deniedState;
	        }
	    }, {
	        key: 'setResourcesAccess',
	        value: function setResourcesAccess(resourcesAccess) {
	            this.resourcesAccess = resourcesAccess;
	        }
	    }, {
	        key: '$get',
	        value: ["$injector", "$state", "$rootScope", "$timeout", function $get($injector, $state, $rootScope, $timeout) {
	            'ngInject';

	            return new KeycloakRBACService($injector, $state, $rootScope, $timeout, this.deniedState, this.resourcesAccess);
	        }]
	    }]);

	    return KeycloakRBACServiceProvider;
	}();

/***/ },
/* 4 */
/***/ function(module, exports) {

	'use strict';

	keycloakHasAccess.$inject = ["ngIfDirective", "keycloakRBACService"];
	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.keycloakHasAccess = keycloakHasAccess;
	function keycloakHasAccess(ngIfDirective, keycloakRBACService) {
	    'ngInject';

	    var ngIf = ngIfDirective[0];

	    return {
	        transclude: ngIf.transclude,
	        priority: ngIf.priority,
	        terminal: ngIf.terminal,
	        restrict: ngIf.restrict,
	        link: function link($scope, $element, $attr) {
	            var hasAccess = keycloakRBACService.hasAccessToResource($attr.bootstartHasAccess);
	            $attr.ngIf = function () {
	                return hasAccess;
	            };
	            ngIf.link.apply(ngIf, arguments);
	        }
	    };
	};

/***/ }
/******/ ]);