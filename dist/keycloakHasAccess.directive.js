/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(4);


/***/ },
/* 1 */,
/* 2 */,
/* 3 */,
/* 4 */
/***/ function(module, exports) {

	'use strict';

	keycloakHasAccess.$inject = ["ngIfDirective", "keycloakRBACService"];
	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.keycloakHasAccess = keycloakHasAccess;
	function keycloakHasAccess(ngIfDirective, keycloakRBACService) {
	    'ngInject';

	    var ngIf = ngIfDirective[0];

	    return {
	        transclude: ngIf.transclude,
	        priority: ngIf.priority,
	        terminal: ngIf.terminal,
	        restrict: ngIf.restrict,
	        link: function link($scope, $element, $attr) {
	            var hasAccess = keycloakRBACService.hasAccessToResource($attr.bootstartHasAccess);
	            $attr.ngIf = function () {
	                return hasAccess;
	            };
	            ngIf.link.apply(ngIf, arguments);
	        }
	    };
	};

/***/ }
/******/ ]);